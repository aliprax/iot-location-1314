#include "location.h"
configuration LocationAppC {}

implementation {

  components MainC;
  components LocationC as App;
  components new AMSenderC(AM_LOCATION);
  
  components ActiveMessageC;
 // components new PositionC();
  components new TimerMilliC() as ATimer;
  components new TimerMilliC() as MTimer;

  //Boot interface
  App.Boot -> MainC.Boot;

  //Send and Receive interfaces
  
  App.AMSend -> AMSenderC;

  //Radio Control
  App.AMControl -> ActiveMessageC;

  //Interfaces to access pck fields
  App.Packet -> AMSenderC;

  //Timer interface
  App.AnchorTimer -> ATimer;
  App.MoteTimer -> MTimer;

  components PositionC;
  App.Position -> PositionC;
  App.PositionControl -> PositionC;
  
}

