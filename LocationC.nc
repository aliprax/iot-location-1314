#include "location.h"


module LocationC {

uses {
	interface Boot;
  interface Timer<TMilli> as AnchorTimer;

  interface Packet;
  interface AMSend;
  
  interface SplitControl as AMControl;

  interface Timer<TMilli> as MoteTimer;

  interface Position;
  interface StdControl as PositionControl;
}

} implementation {

  message_t pkt;
  bool busy=FALSE;
  uint32_t sec=0;

  event void Boot.booted(){
   // dbg("default","%s | NODE %d started\n", sim_time_string(), TOS_NODE_ID);
    if(TOS_NODE_ID==0){
      call PositionControl.start();
    }
    call AMControl.start();

    
  }

  event void AMControl.startDone(error_t err) {
  //  dbg("default","%s | NODE %d AMControl.startDone()\n", sim_time_string(), TOS_NODE_ID);
      if (err == SUCCESS) {
        if(TOS_NODE_ID!=0){
          call AnchorTimer.startPeriodic(250);
        }else{
          call MoteTimer.startPeriodic(1000);
        }
      } else {
        call AMControl.start();
    }
  }


  event void AMControl.stopDone(error_t res){}

  event void AnchorTimer.fired(){
    uint32_t x;
    uint32_t y; 
//    dbg("default","%s | ANCHOR %d anchor timer fired\n",sim_time_string(),TOS_NODE_ID);
    if(!busy){      
      AnchorMsg* msg = (AnchorMsg*)(call Packet.getPayload(&pkt,sizeof(AnchorMsg)));
      *(float*)&x=getAnchorX(TOS_NODE_ID);
      *(float*)&y=getAnchorY(TOS_NODE_ID);
      dbg_clear("default","ANCHOR %d sending position x:%g y:%g\n",TOS_NODE_ID,*(float*)&x,*(float*)&y);
      msg->x = x;
      msg->y = y;
      if (call AMSend.send(AM_BROADCAST_ADDR,&pkt, sizeof(AnchorMsg)) == SUCCESS) {
            busy = TRUE;
           // dbg("default","%s | ANCHOR %d message sent\n",sim_time_string(),TOS_NODE_ID);
      }
    }
  }

  event void AMSend.sendDone(message_t* msg, error_t err) {
    if (&pkt == msg) busy = FALSE;
   // dbg("default","%s | ANCHOR %d: send done\n",sim_time_string(),TOS_NODE_ID);
    if(err==FAIL){
      dbgerror("error", "%s | ANCHOR %d: error send has failed!!!\n", sim_time_string(),TOS_NODE_ID);
    }
  }
  

  event void MoteTimer.fired(){
    coord_t position = call Position.getPosition();
    dbg_clear("default","\nMOTE LOCATION estimated coordinates: (%g,%g)\n\n", position.x,position.y);
    dbg_clear("data4log", "%g,%g\n",position.x,position.y);
  }

}

