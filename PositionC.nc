configuration PositionC{
  provides interface Position;
  provides interface StdControl;
}
implementation{
  components PositionP;
  Position=PositionP;
  StdControl=PositionP;

  components new AMReceiverC(AM_LOCATION);
  PositionP.Receive->AMReceiverC;

  components RSSEstimatorC;
  PositionP.RSSEstimator->RSSEstimatorC;
  PositionP.RSSControl->RSSEstimatorC;

}
