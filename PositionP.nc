#include "location.h"
#include "math.h"
#define P0 -60.0

module PositionP {
  provides{
    interface Position;
    interface StdControl;
  }
  uses{

    interface Receive;
    interface RSSEstimator;
    interface StdControl as RSSControl;
  }
}
implementation{
  coord_t last;
  coord_t coords[3];
  float dist[3];
  float ss[3]={-900.0,-900.0,-900.0};




  command error_t StdControl.start(){
    last.x=1.0;
    last.y=1.0;
    coords[0].x=0;coords[0].y=0;
    coords[1].x=1;coords[1].y=1;
    coords[2].x=2;coords[2].y=2;
    return call RSSControl.start();
  }

  command error_t StdControl.stop(){
    return call RSSControl.stop();
  }

  void storeCoordinates(coord_t c_tmp,float ss_tmp){
    int i=0;
    int stored=0;
    //dbg_clear("store","-----------------storeCoordinates-------------------\n");
    dbg_clear("store","param: (%g,%g) %g %g\n", c_tmp.x,c_tmp.y,0.0,ss_tmp);
    dbg_clear("store","slot0: (%g,%g) %g %g\n", coords[0].x,coords[0].y,dist[0],ss[0]);
    dbg_clear("store","slot1: (%g,%g) %g %g\n", coords[1].x,coords[1].y,dist[1],ss[1]);
    dbg_clear("store","slot2: (%g,%g) %g %g\n", coords[2].x,coords[2].y,dist[2],ss[2]);
    for(i=0; i<3;i++){//substitute existing values
      if(coords[i].x == c_tmp.x && coords[i].y == c_tmp.y){
        dbg_clear("store","substituted existing values in slot %d\n",i);
        stored=1;
        break;
      }
    }
    if( stored==0){//substitute with weaker signal
      for(i=0;i<3;i++){
        if (ss_tmp > ss[i]){
          dbg_clear("store","substituted weaker signal in slot %d \n", i);
          break;
        }
      }
    }
    if(i<3){
      ss[i]=ss_tmp;
      coords[i]=c_tmp;
      dist[i]= pow(10.0,(P0-ss_tmp)/10.0);
      dbg_clear("store","stored anchor data: (%g,%g) dist:%g rss:%g\n", c_tmp.x,c_tmp.y,dist[i],ss_tmp);
    }else{
      dbg_clear("store", "data not stored\n");
    }
  }

  void onAnchorMsgReceived(AnchorMsg* anchor_msg){
    uint32_t x=anchor_msg->x;
    uint32_t y=anchor_msg->y;
    coord_t c_tmp;
    float ss_tmp;
    c_tmp.x=*(float*)&x;
    c_tmp.y=*(float*)&y;
    dbg_clear("message","___________________________Anchor_Message____________________\n");
    dbg_clear("message", "MOTE POSITION : anchor message received x:%g,y:%g\n",c_tmp.x,c_tmp.y);
    ss_tmp = call RSSEstimator.getRSSI(anchor_msg);
    storeCoordinates(c_tmp,ss_tmp);
  }

  event message_t* Receive.receive(message_t* msg, void* payload, uint8_t len){
//    dbg("default","%s | NODE %d: message received\n",sim_time_string(),TOS_NODE_ID);
    if (TOS_NODE_ID==0 && len==sizeof(AnchorMsg)){
      AnchorMsg* anchor_msg= (AnchorMsg*) payload;
      onAnchorMsgReceived(anchor_msg);
    }
    return msg;
  }

  float distance(coord_t* c1,coord_t* c2){
    return sqrt( pow(c1->x - c2->x,2) + pow(c1->y - c2->y,2) );
  }

  float objectiveFunction(coord_t* c){
    int i;
    float acc;
    for(i=0, acc=0.0;i<3;i++){
      acc += pow( distance(c,&coords[i]) - dist[i],2);
    }
    acc=acc/2.0;
    dbg_clear("gradient","obj function %g\n", acc);
    return acc;
  }

  void updateStepOn(coord_t* c){
    float alpha=0.4;
    int i;
    float accX,accY;
    for (i=0,accX=0.0,accY=0.0;i<3;i++){
      accX+=(1.0- dist[i]/distance(c,&coords[i]))*(c->x-coords[i].x);
      accY+=(1.0- dist[i]/distance(c,&coords[i]))*(c->y-coords[i].y);
    }
    dbg_clear("gradient","diff x %g\n", accX*alpha);
    dbg_clear("gradient","diff y %g\n", accY*alpha);
    c->x -=  alpha * accX; 
    c->y -=  alpha * accY;
    return;
  }

  coord_t applyGradientDescent(){
    coord_t val=last;
    int count=0;
    float obj_fun_prev= objectiveFunction(&val);
    float obj_fun_new=obj_fun_prev+1;
    dbg_clear("gradient", "\n\n________________________Gradient_Descent___________________________\n");
    dbg_clear("gradient","start with (%g,%g)\n",val.x,val.y);

    while(obj_fun_new > 0.00001 && obj_fun_new!=obj_fun_prev && count<100 ){
      updateStepOn(&val);
      count++;
      obj_fun_prev=obj_fun_new;
      obj_fun_new=objectiveFunction(&val);
      dbg_clear("gradient", "next step value : (%g,%g)\n", val.x,val.y);

    }
    last=val;
    return val;
  }

  command coord_t Position.getPosition(){
    coord_t res;
    res= applyGradientDescent();
    return res;

  }

}