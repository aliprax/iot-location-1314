configuration RSSEstimatorC{
  provides interface RSSEstimator;
  provides interface StdControl;
}
implementation{
  components RSSEstimatorP;
  components new TimerMilliC() as MoveTimer;

  RSSEstimator=RSSEstimatorP;
  StdControl=RSSEstimatorP;
  RSSEstimatorP.MoveTimer -> MoveTimer;
}