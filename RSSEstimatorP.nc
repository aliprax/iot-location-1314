#include "location.h"
#include "math.h"
#define P0 -60.0
#define TWO_PI 6.2831853071795864769252866
#define VARIANCE 1.0

module RSSEstimatorP{
  provides interface StdControl;
  provides interface RSSEstimator;
  uses interface Timer<TMilli> as MoveTimer;

}
implementation{

  uint8_t sec=0; //goes from 0 to 9 and back: index for position array located in location.h
  uint8_t getMoveTime(){
    if(sec>=9){
      return 18-sec;
    }else{
      return sec;
    }
  }
  command error_t StdControl.start(){
    dbg_clear("rss", "\n/************************************************************/\n");
    dbg_clear("rss", "/***                    NEW ITERATION                     ***/\n");
    dbg_clear("rss", "/************************************************************/\n");
    dbg_clear("rss","MOTE RSS new position (%g,%g)\n",getMobileX(getMoveTime()),getMobileY(getMoveTime()));
    dbg_clear("data4log","%g,%g\n",getMobileX(getMoveTime()),getMobileY(getMoveTime()));
    call MoveTimer.startPeriodic(1000);
    return SUCCESS;
  }
  event void MoveTimer.fired(){
    uint8_t move_time=0;
    if(sec==18){
      sec=0;
    }else{
      sec++;
    }
    move_time=getMoveTime();
    dbg_clear("rss", "\n/************************************************************/\n");
    dbg_clear("rss", "/***                    NEW ITERATION                     ***/\n");
    dbg_clear("rss", "/************************************************************/\n");
    dbg_clear("rss","MOTE RSS new position (%g,%g)\n",getMobileX(move_time),getMobileY(move_time));
    dbg_clear("data4log","%g,%g\n",getMobileX(move_time),getMobileY(move_time));
  }

 

  command error_t StdControl.stop(){
    call MoveTimer.stop();
    return SUCCESS;
  }



  float computeDistance(coord_t c1,coord_t c2){
    float dist;   
    dist = (float) sqrt(pow(c1.x-c2.x,2) + pow(c1.y-c2.y,2));
    return dist;
  }

 
  double generateGaussianNoise(){
    double variance = VARIANCE ;
    static int hasSpare = 0;
    static double rand1, rand2;
   
    if(hasSpare)
    {
      hasSpare = 0;
      return sqrt(variance * rand1) * sin(rand2);
    }
   
    hasSpare = 1;
   
    rand1 = rand() / ((double) RAND_MAX);
    if(rand1 < 1e-100) rand1 = 1e-100;
    rand1 = -2 * log(rand1);
    rand2 = (rand() / ((double) RAND_MAX)) * TWO_PI;
   
    return sqrt(variance * rand1) * cos(rand2);
  }


  float noise(){
    return generateGaussianNoise();
  }


  
  float computeRSSI(float distance){
    float P;
    float n;
    float simP;  
    P = P0 - 10 * log10(distance);
    n = noise();
    simP=P+n;
    dbg_clear("rss","MOTE RSS P:%g P0:%g n:%g -> simulated P:%g\n",P,P0,n,simP);
    return simP;
  }


  command float RSSEstimator.getRSSI(AnchorMsg* anchor_msg){
    uint32_t _x;
    uint32_t _y;
    coord_t c_anchor;
    coord_t c_mote;
    float dist;
    float simP;
    _x=anchor_msg->x;
    _y=anchor_msg->y;
    c_anchor.x=*(float*)&_x;
    c_anchor.y=*(float*)&_y;
    c_mote.x=getMobileX(getMoveTime());
    c_mote.y=getMobileY(getMoveTime());
    dist = computeDistance(c_anchor,c_mote);
    simP = computeRSSI(dist);
    return simP;
  }
  
}