def self.compute_average_error(std_dev,file_name)
  file = File.open(file_name, "r")
  dim=0
  real=[]
  estimated=[]
  error=[]

  while not file.eof?
    line =file.readline
    real<<line
    if(not file.eof?)
      dim=dim+1
      estimated<<file.readline
    else 
      break
    end
  end
  
  #calcoli
  i=0
  while i < dim
    xe=estimated[i].split(",").first.to_f
    xr=real[i].split(",").first.to_f
    ye=estimated[i].split(",").last.to_f
    yr=real[i].split(",").last.to_f
    e=Math.sqrt((xe-xr)*(xe-xr)+(ye-yr)*(ye-yr))
    error<<e
    i=i+1
  end
  average=0
  average = error.inject 0.0 do|mem,var|
    mem+var*var
  end
  average=average/i
  puts ("#{std_dev} " + average.to_s);
end


compute_average_error(0,'output0.txt')
compute_average_error(0.5,'output05.txt')
compute_average_error(1,'output1.txt')
compute_average_error(5,'output5.txt')
compute_average_error(10,'output10.txt')
compute_average_error(20,'output20.txt')
compute_average_error(30,'output30.txt')
compute_average_error(50,'output50.txt')
