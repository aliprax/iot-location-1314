#ifndef LOCATION_H
#define LOCATION_H

enum{
  AM_LOCATION=6
};

typedef nx_struct AnchorMsg_s{
  nx_uint32_t x;
  nx_uint32_t y;
}AnchorMsg;

typedef struct coord_s{
  float x;
  float y;
}coord_t;

float anchor[8][2]= {                    
                    {0.0,0.0},//anchor1
                    {2.0,0.0},//anchor2
                    {4.0,0.0},//anchor3
                    {6.0,0.0},//anchor4
                    {0.0,3.0},//anchor5
                    {2.0,3.0},//anchor6
                    {4.0,3.0},//anchor7
                    {6.0,3.0}//anchor8
                    };

  float move[10][2] = {
                    {1.0,1.0},//sec0
                    {1.5,1.5},//sec1
                    {2.0,2.0},//sec2
                    {2.5,1.5},//sec3
                    {3.0,1.0},//sec4
                    {3.5,1.5},//sec5
                    {4.0,2.0},//sec6
                    {4.5,1.5},//sec7
                    {5.0,2.0},//sec8
                    {5.5,1.5}//sec9
                    };

  float getAnchorX(int nodeid){
    return anchor[nodeid-1][0];
  }

  float getAnchorY(int nodeid){
     return anchor[nodeid-1][1];
  }

  float getMobileX(int sec){
    return move[sec][0];
  
  }

  float getMobileY(uint16_t sec){
    return move[sec][1];

  }

#endif